#!/bin/sh

set -xe

clang -O3 -mavx2 -Wall -Wextra -pedantic -std=c99 -ggdb -o gfx gfx.c -lX11 -lm
