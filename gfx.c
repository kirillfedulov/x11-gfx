#define _DEFAULT_SOURCE /* to get struct timespec definition from time.h */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include <math.h>

typedef uint64_t uint64;
typedef uint32_t uint32;
typedef uint16_t uint16;
typedef uint8_t  uint8;

typedef int64_t int64;
typedef int32_t int32;
typedef int16_t int16;
typedef int8_t  int8;

typedef uint32_t uint;
typedef uint8_t  byte;

typedef float  float32;
typedef double float64;

#include <x86intrin.h>
#include <immintrin.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>

#define STB_IMAGE_IMPLEMENTATION
#define STBI_ONLY_PNG
#include "./stb_image.h"

uint32 colorEncodeBGRA(byte r, byte g, byte b, byte a) {
	uint32 result;

	result = (a << 24) | (r << 16) | (g << 8) | b;
	return result;
}

void colorDecodeBGRA(uint32 color, byte *r, byte *g, byte *b, byte *a) {
	*r = (color >> 16) & 0xFF;
	*g = (color >> 8)  & 0xFF;
	*b = (color)       & 0xFF;
	*a = (color >> 24) & 0xFF;
}

uint32 colorConvertRGBAToBGRA(uint32 a) {
	uint32 result;

	result = (((a) & 0xFF) << 16) | (((a >> 8) & 0xFF) << 8) | (((a >> 16) & 0xFF)) | (((a >> 24) & 0xFF) << 24);
	return result;
}

uint uintMax(uint a, uint b) {
	uint result;

	result = a > b ? a : b;
	return result;
}

uint uintMin(uint a, uint b) {
	uint result;

	result = a < b ? a : b;
	return result;
}

bool rectGetIntersection(uint x1, uint y1, uint w1, uint h1, uint x2, uint y2, uint w2, uint h2, uint *x, uint *y, uint *w, uint *h) {
	uint ix1, ix2, iy1, iy2;
	bool result;

	result = false;

	ix1 = uintMax(x1, x2);
	ix2 = uintMin(x1 + w1, x2 + w2);
	if (ix1 < ix2) {
		iy1 = uintMax(y1, y2);
		iy2 = uintMin(y1 + h1, y2 + h2);
		if (iy1 < iy2) {
			*x = ix1;
			*y = iy1;
			*w = ix2 - ix1;
			*h = iy2 - iy1;
			result = true;
		}
	}

	return result;
}

enum Pixel_Format {
	PIXEL_FORMAT_RGBA,
	PIXEL_FORMAT_RGBX,

	PIXEL_FORMAT_BGRA,
	PIXEL_FORMAT_BGRX,
};
typedef enum Pixel_Format Pixel_Format;

struct Texture {
	uint width;
	uint height;

	uint bpp;
	uint pitch;

	Pixel_Format format;

	uint32 *pixels;
};
typedef struct Texture Texture;

Texture textureCreateBGRA(uint width, uint height) {
	Texture result;

	result.width  = width;
	result.height = height;
	result.bpp = 4;
	result.pitch = width * result.bpp;
	result.format = PIXEL_FORMAT_BGRA;
	result.pixels = malloc(result.pitch * result.height);
	return result;
}

Texture textureLoadPNG(const char *path) {
	int width, height, bpp;
	byte *pixels;
	Texture result;

	pixels = stbi_load(path, &width, &height, &bpp, STBI_rgb_alpha);
	assert(pixels);
	assert(bpp == 4 || bpp == 3);

	result.width = width;
	result.height = height;
	result.bpp = 4;
	result.pitch = width * result.bpp;
	result.format = bpp == 4 ? PIXEL_FORMAT_RGBA : PIXEL_FORMAT_RGBX;
	result.pixels = pixels;
	return result;
}

void textureResize(Texture *texture, uint width, uint height) {
	texture->width = width;
	texture->height = height;
	texture->pitch = width * texture->bpp;
	texture->pixels = realloc(texture->pixels, texture->pitch * height);
}

uint32 colorEncodePixel(Pixel_Format format, uint r, uint g, uint b, uint a) {
	uint32 result = 0;

	switch (format) {
		case PIXEL_FORMAT_RGBA:
			result = ((r & 0xff) << 0) | ((g & 0xff) << 8) | ((b & 0xff) << 16) | ((a & 0xff) << 24);
			break;
		case PIXEL_FORMAT_RGBX:
			result = ((r & 0xff) << 0) | ((g & 0xff) << 8) | ((b & 0xff) << 16);
			break;
		case PIXEL_FORMAT_BGRA:
			result = ((r & 0xff) << 16) | ((g & 0xff) << 8) | ((b & 0xff) << 0) | ((a & 0xff) << 24);
			break;
		case PIXEL_FORMAT_BGRX:
			result = ((r & 0xff) << 16) | ((g & 0xff) << 8) | ((b & 0xff) << 0);
			break;
		default:
			assert(0);
	}

	return result;
}

__m256i colorEncodePixel256(Pixel_Format format, __m256 r, __m256i g, __m256i b, __m256i a) {
	__m256i result;

	switch (format) {
		case PIXEL_FORMAT_RGBA:
			result = _mm256_or_si256(r, _mm256_or_si256(_mm256_slli_epi32(g, 8),
						                    _mm256_or_si256(_mm256_slli_epi32(b, 16),
									            _mm256_slli_epi32(a, 24))));
			break;
		case PIXEL_FORMAT_RGBX:
			result = _mm256_or_si256(r, _mm256_or_si256(_mm256_slli_epi32(g, 8),
						                    _mm256_slli_epi32(b, 16)));
			break;
		case PIXEL_FORMAT_BGRA:
			result = _mm256_or_si256(_mm256_slli_epi32(r, 16),
				                 _mm256_or_si256(_mm256_slli_epi32(g, 8),
							         _mm256_or_si256(b, _mm256_slli_epi32(a, 24))));
			break;
		case PIXEL_FORMAT_BGRX:
			result = _mm256_or_si256(_mm256_slli_epi32(r, 16),
				                 _mm256_or_si256(_mm256_slli_epi32(g, 8), b));
			break;
		default:
			assert(0);
	}

	return result;
}

void colorDecodePixel(Pixel_Format format, uint32 pixel, uint *r, uint *g, uint *b, uint *a) {
	switch (format) {
		case PIXEL_FORMAT_RGBA:
			*r = (pixel >> 0) & 0xff;
			*g = (pixel >> 8) & 0xff;
			*b = (pixel >> 16) & 0xff;
			*a = (pixel >> 24) & 0xff;
			break;
		case PIXEL_FORMAT_RGBX:
			*r = (pixel >> 0) & 0xff;
			*g = (pixel >> 8) & 0xff;
			*b = (pixel >> 16) & 0xff;
			*a = 0;
			break;
		case PIXEL_FORMAT_BGRA:
			*r = (pixel >> 16) & 0xff;
			*g = (pixel >> 8) & 0xff;
			*b = (pixel >> 0) & 0xff;
			*a = (pixel >> 24) & 0xff;
			break;
		case PIXEL_FORMAT_BGRX:
			*r = (pixel >> 16) & 0xff;
			*g = (pixel >> 8) & 0xff;
			*b = (pixel >> 0) & 0xff;
			*a = 0;
			break;
		default:
			assert(0);
	}
}

void colorDecodePixel256(Pixel_Format format, __m256i pixel, __m256i *r, __m256i *g, __m256i *b, __m256i *a) {
	__m256i mask = _mm256_set1_epi32(0xff);

	switch (format) {
		case PIXEL_FORMAT_RGBA:
			*r = _mm256_and_si256(_mm256_srli_epi32(pixel, 0), mask);
			*g = _mm256_and_si256(_mm256_srli_epi32(pixel, 8), mask);
			*b = _mm256_and_si256(_mm256_srli_epi32(pixel, 16), mask);
			*a = _mm256_and_si256(_mm256_srli_epi32(pixel, 24), mask);
			break;
		case PIXEL_FORMAT_RGBX:
			*r = _mm256_and_si256(_mm256_srli_epi32(pixel, 0), mask);
			*g = _mm256_and_si256(_mm256_srli_epi32(pixel, 8), mask);
			*b = _mm256_and_si256(_mm256_srli_epi32(pixel, 16), mask);
			*a = _mm256_setzero_si256();
			break;
		case PIXEL_FORMAT_BGRA:
			*r = _mm256_and_si256(_mm256_srli_epi32(pixel, 16), mask);
			*g = _mm256_and_si256(_mm256_srli_epi32(pixel, 8), mask);
			*b = _mm256_and_si256(_mm256_srli_epi32(pixel, 0), mask);
			*a = _mm256_and_si256(_mm256_srli_epi32(pixel, 24), mask);
			break;
		case PIXEL_FORMAT_BGRX:
			*r = _mm256_and_si256(_mm256_srli_epi32(pixel, 16), mask);
			*g = _mm256_and_si256(_mm256_srli_epi32(pixel, 8), mask);
			*b = _mm256_and_si256(_mm256_srli_epi32(pixel, 0), mask);
			*a = _mm256_setzero_si256();
			break;
		default:
			assert(0);
	}
}

uint32 colorConvertPixel(Pixel_Format from, Pixel_Format to, uint32 pixel) {
	uint32 result = 0;

	switch (from) {
		case PIXEL_FORMAT_RGBA:
			switch (to) {
				case PIXEL_FORMAT_RGBA:
					result = pixel;
					break;
				case PIXEL_FORMAT_RGBX:
					result = pixel & 0x00ffffff;
					break;
				case PIXEL_FORMAT_BGRA:
					result = (((pixel >> 0) & 0xff) << 16) | (((pixel >> 8) & 0xff) << 8) | (((pixel >> 16) & 0xff) << 0) | (((pixel >> 24) & 0xff) << 24);
					break;
				case PIXEL_FORMAT_BGRX:
					result = (((pixel >> 0) & 0xff) << 16) | (((pixel >> 8) & 0xff) << 8) | (((pixel >> 16) & 0xff) << 0);
					break;
				default:
					assert(0);
			}
			break;
		case PIXEL_FORMAT_RGBX:
			switch (to) {
				case PIXEL_FORMAT_RGBA:
				case PIXEL_FORMAT_RGBX:
					result = pixel;
					break;
				case PIXEL_FORMAT_BGRA:
				case PIXEL_FORMAT_BGRX:
					result = (((pixel >> 0) & 0xff) << 16) | (((pixel >> 8) & 0xff) << 8) | (((pixel >> 16) & 0xff) << 0);
					break;
				default:
					assert(0);
			}
			break;
		case PIXEL_FORMAT_BGRA:
			switch (to) {
				case PIXEL_FORMAT_RGBA:
					result = (((pixel >> 16) & 0xff) << 0) | (((pixel >> 8) & 0xff) << 8) | (((pixel >> 0) & 0xff) << 16) | (((pixel >> 24) & 0xff) << 24);
					break;
				case PIXEL_FORMAT_RGBX:
					result = (((pixel >> 16) & 0xff) << 0) | (((pixel >> 8) & 0xff) << 8) | (((pixel >> 0) & 0xff) << 16);
					break;
				case PIXEL_FORMAT_BGRA:
					result = pixel;
					break;
				case PIXEL_FORMAT_BGRX:
					result = pixel & 0x00ffffff;
					break;
				default:
					assert(0);
			}
			break;
		case PIXEL_FORMAT_BGRX:
			switch (to) {
				case PIXEL_FORMAT_RGBA:
				case PIXEL_FORMAT_RGBX:
					result = (((pixel >> 16) & 0xff) << 0) | (((pixel >> 8) & 0xff) << 8) | (((pixel >> 0) & 0xff) << 16);
					break;
				case PIXEL_FORMAT_BGRA:
				case PIXEL_FORMAT_BGRX:
					result = pixel;
					break;
				default:
					assert(0);
			}
			break;
		default:
			assert(0);
	}

	return result;
}

__m256i colorConvertPixel256(Pixel_Format from, Pixel_Format to, __m256i pixel) {
	byte noalpha[32] = { 0x70, 0x71, 0x72, 0x00, 0x74, 0x75, 0x76, 0x00, 0x78, 0x79, 0x7a,
		0x00, 0x7c, 0x7d, 0x7e, 0x00, 0x70, 0x71, 0x72, 0x00, 0x74, 0x75, 0x76, 0x00, 0x78, 0x79, 0x7a, 0x00, 0x7c, 0x7d, 0x7e, 0x00 };
	byte shift[32] = { 0x72, 0x71, 0x70, 0x73, 0x76, 0x75, 0x74, 0x77, 0x7a, 0x79, 0x78, 0x7b, 0x7e,
		0x7d, 0x7c, 0x7f, 0x72, 0x71, 0x70, 0x73, 0x76, 0x75, 0x74, 0x77, 0x7a, 0x79, 0x78, 0x7b, 0x7e, 0x7d, 0x7c, 0x7f };
	byte shift_noalpha[32] = { 0x72, 0x71, 0x70, 0x00, 0x76, 0x75, 0x74, 0x00, 0x7a, 0x79, 0x78, 0x00,
		0x7e, 0x7d, 0x7c, 0x00, 0x72, 0x71, 0x70, 0x00, 0x76, 0x75, 0x74, 0x00, 0x7a, 0x79, 0x78, 0x00, 0x7e, 0x7d, 0x7c, 0x00 };

	__m256i result;

	switch (from) {
		case PIXEL_FORMAT_RGBA:
			switch (to) {
				case PIXEL_FORMAT_RGBA:
					result = pixel;
					break;
				case PIXEL_FORMAT_RGBX:
					result = _mm256_shuffle_epi8(pixel, _mm256_loadu_si256((const __m256i *) noalpha));
					break;
				case PIXEL_FORMAT_BGRA:
					result = _mm256_shuffle_epi8(pixel, _mm256_loadu_si256((const __m256i *) shift));
					break;
				case PIXEL_FORMAT_BGRX:
					result = _mm256_shuffle_epi8(pixel, _mm256_loadu_si256((const __m256i *) shift_noalpha));
					break;
				default:
					assert(0);
			}
			break;
		case PIXEL_FORMAT_RGBX:
			switch (to) {
				case PIXEL_FORMAT_RGBA:
				case PIXEL_FORMAT_RGBX:
					result = pixel;
					break;
				case PIXEL_FORMAT_BGRA:
				case PIXEL_FORMAT_BGRX:
					result = _mm256_shuffle_epi8(pixel, _mm256_loadu_si256((const __m256i *) shift_noalpha));
					break;
				default:
					assert(0);
			}
			break;
		case PIXEL_FORMAT_BGRA:
			switch (to) {
				case PIXEL_FORMAT_RGBA:
					result = _mm256_shuffle_epi8(pixel, _mm256_loadu_si256((const __m256i *) shift));
					break;
				case PIXEL_FORMAT_RGBX:
					result = _mm256_shuffle_epi8(pixel, _mm256_loadu_si256((const __m256i *) shift_noalpha));
					break;
				case PIXEL_FORMAT_BGRA:
					result = pixel;
					break;
				case PIXEL_FORMAT_BGRX:
					result = _mm256_shuffle_epi8(pixel, _mm256_loadu_si256((const __m256i *) noalpha));
					break;
				default:
					assert(0);
			}
			break;
		case PIXEL_FORMAT_BGRX:
			switch (to) {
				case PIXEL_FORMAT_RGBA:
				case PIXEL_FORMAT_RGBX:
					result = _mm256_shuffle_epi8(pixel, _mm256_loadu_si256((const __m256i *) shift_noalpha));
					break;
				case PIXEL_FORMAT_BGRA:
				case PIXEL_FORMAT_BGRX:
					result = pixel;
					break;
				default:
					assert(0);
			}
			break;
		default:
			assert(0);
	}

	return result;
}

void colorMulBlend(uint sr, uint sg, uint sb, uint sa, uint *dr, uint *dg, uint *db, uint *da) {
	uint inv_sa = 255 - sa;

	*dr = (inv_sa * *dr + sa * sr + 1) >> 8;
	*dg = (inv_sa * *dg + sa * sg + 1) >> 8;
	*db = (inv_sa * *db + sa * sb + 1) >> 8;
	/*
	 * TODO(kirill): what should i do here?
	 */
	*da = 255;
}

void colorMulBlend256(__m256i sr, __m256i sg, __m256i sb, __m256i sa, __m256i *dr, __m256i *dg, __m256i *db, __m256i *da) {
	__m256i _1 = _mm256_set1_epi32(1), _255 = _mm256_set1_epi32(255);
	__m256i inv_sa = _mm256_sub_epi32(_255, sa);
	
	*dr = _mm256_srli_epi32(_mm256_add_epi32(_mm256_add_epi32(_mm256_mullo_epi32(inv_sa, *dr), _mm256_mullo_epi32(sa, sr)), _1), 8);
	*dg = _mm256_srli_epi32(_mm256_add_epi32(_mm256_add_epi32(_mm256_mullo_epi32(inv_sa, *dg), _mm256_mullo_epi32(sa, sg)), _1), 8);
	*db = _mm256_srli_epi32(_mm256_add_epi32(_mm256_add_epi32(_mm256_mullo_epi32(inv_sa, *db), _mm256_mullo_epi32(sa, sb)), _1), 8);
	/*
	 * TODO(kirill): what should i do here?
	 */
	*da = _255;
}

void textureFillWithColor(Texture *dst, uint r, uint g, uint b, uint a) {
	byte *dp = dst->pixels;
	uint32 color = colorEncodePixel(dst->format, r, g, b, a);
	__m256i color256 = _mm256_set1_epi32(color);

	uint i, n = dst->width * dst->height;
#if 1
	for (i = 0; i + 8 < n; i += 8, dp += 8 * dst->bpp) {
		_mm256_storeu_si256((__m256i *) dp, color256);
	}
#else
	i = 0;
#endif
	for (; i < n; ++i, dp += dst->bpp) {
		*(uint32 *) dp = color;
	}
}

void textureFillRectWithColor(Texture *dst, uint x, uint y, uint w, uint h, uint r, uint g, uint b, uint a) {
	if (rectGetIntersection(0, 0, dst->width, dst->height, x, y, w, h, &x, &y, &w, &h)) {
		byte *dl = (byte *)dst->pixels + y * dst->pitch + x * dst->bpp;
		uint32 color = colorEncodePixel(dst->format, r, g, b, a);
		__m256 color256 = _mm256_set1_epi32(color);

		uint i, j;
		for (i = 0; i < h; ++i, dl += dst->pitch) {
			byte *dp = dl;

#if 1
			for (j = 0; j + 8 < w; j += 8, dp += 8 * dst->bpp) {
				_mm256_storeu_si256((__m256i *) dp, color256);
			}
#else
			j = 0;
#endif
			for (; j < w; ++j, dp += dst->bpp) {
				*(uint32 *) dp = color;
			}
		}
	}
}

void textureFillRectWithTexture(Texture *dst, uint x, uint y, uint w, uint h,
		                Texture *src) {
	if (w == 0 || w > src->width) {
		w = src->width;
	}
	if (h == 0 || h > src->height) {
		h = src->height;
	}
	if (rectGetIntersection(0, 0, dst->width, dst->height, x, y, w, h, &x, &y, &w, &h)) {
		byte *sl = src->pixels;
		byte *dl = (byte*)dst->pixels + y * dst->pitch + x * dst->bpp;

		uint i, j;
		for (i = 0; i < h; ++i, sl += src->pitch, dl += dst->pitch) {
			byte *sp = sl;
			byte *dp = dl;

#if 1
			for (j = 0; j + 8 < w; j += 8, sp += 8 * src->bpp, dp += 8 * dst->bpp) {
				__m256i spixel = colorConvertPixel256(src->format, dst->format,
						                      _mm256_loadu_si256((const __m256i *) sp));
				_mm256_storeu_si256((__m256i *) dp, spixel);
			}
#else
			j = 0;
#endif
			for (; j < w; ++j, sp += src->bpp, dp += dst->bpp) {
				*(uint32 *) dp = colorConvertPixel(src->format, dst->format, *(uint32 *) sp);
			}
		}
	}
}

void textureFillRectWithTextureBlended(Texture *dst, uint x, uint y, uint w, uint h,
		                       Texture *src) {
	if (w == 0 || w > src->width) {
		w = src->width;
	}
	if (h == 0 || h > src->height) {
		h = src->height;
	}
	if (rectGetIntersection(0, 0, dst->width, dst->height, x, y, w, h, &x, &y, &w, &h)) {
		byte *sl = src->pixels;
		byte *dl = (byte*)dst->pixels + y * dst->pitch + x * dst->bpp;

		uint i, j;
		for (i = 0; i < h; ++i, sl += src->pitch, dl += dst->pitch) {
			byte *sp = sl;
			byte *dp = dl;

#if 1
			for (j = 0; j + 8 < w; j += 8, sp += 8 * src->bpp, dp += 8 * dst->bpp) {
				__m256i sr, sg, sb, sa;
				__m256i dr, dg, db, da;

				colorDecodePixel256(src->format, _mm256_loadu_si256((const __m256i *) sp), &sr, &sg, &sb, &sa);
				colorDecodePixel256(dst->format, _mm256_loadu_si256((const __m256i *) dp), &dr, &dg, &db, &da);
				colorMulBlend256(sr, sg, sb, sa, &dr, &dg, &db, &da);
				_mm256_storeu_si256((__m256i *) dp, colorEncodePixel256(dst->format, dr, dg, db, da));
			}
#else
			j = 0;
#endif

			for (; j < w; ++j, sp += src->bpp, dp += dst->bpp) {
				uint sr, sg, sb, sa;
				uint dr, dg, db, da;

				colorDecodePixel(src->format, *(uint32 *) sp, &sr, &sg, &sb, &sa);
				colorDecodePixel(dst->format, *(uint32 *) dp, &dr, &dg, &db, &da);
				colorMulBlend(sr, sg, sb, sa, &dr, &dg, &db, &da);
				*(uint32 *) dp = colorEncodePixel(dst->format, dr, dg, db, da);
			}
		}
	}
}

void textureFillRectWithTextureScaled(Texture *dst_texture, uint dst_x, uint dst_y,  int _dst_w,  int _dst_h,
		                      Texture *src_texture, uint src_x, uint src_y, uint src_w, uint src_h,
				      bool smooth) {
	bool flipx, flipy;
	uint dst_w, dst_h;
	if (_dst_w < 0) {
		flipx = true;
		dst_w = -_dst_w;
	} else {
		flipx = false;
		dst_w = _dst_w;
	}
	if (_dst_h < 0) {
		flipy = true;
		dst_h = -_dst_h;
	} else {
		flipy = false;
		dst_h = _dst_h;
	}

	if (dst_w == 0) dst_w = src_texture->width;
	if (dst_h == 0) dst_h = src_texture->height;
	if (src_w == 0) src_w = src_texture->width;
	if (src_h == 0) src_h = src_texture->height;

	uint orig_w = dst_w;
	uint orig_h = dst_h;

	if (rectGetIntersection(0, 0, dst_texture->width, dst_texture->height, dst_x, dst_y, dst_w, dst_h, &dst_x, &dst_y, &dst_w, &dst_h)) {
		if (rectGetIntersection(0, 0, src_texture->width, src_texture->height, src_x, src_y, src_w, src_h, &src_x, &src_y, &src_w, &src_h)) {
			uint gapx;
			float32 gapx_f;
			if (src_w > orig_w) {
				gapx_f = (float32) src_w / (float32) orig_w;
				gapx = (uint) gapx_f;
			} else {
				gapx_f = (float32) orig_w / (float32) src_w;
				gapx = (uint) gapx_f;
			}

			uint gapy;
			float32 gapy_f;
			if (src_h > orig_h) {
				gapy_f = (float32) src_h / (float32) orig_h;
				gapy = (uint) gapy_f;
			} else {
				gapy_f = (float32) orig_h / (float32) src_h;
				gapy = (uint) gapy_f;
			}

			uint dst_pitch = dst_texture->width;
			uint src_pitch = src_texture->width;

			uint32 *dst = dst_texture->pixels + dst_pitch * dst_y + dst_x;
			uint32 *src = src_texture->pixels + src_pitch * src_y + src_x;

			uint x, y, dx, dy;
			for (y = 0; y < src_h; ++y) {
				for (x = 0; x < src_w; ++x) {
					if (smooth) {
						uint i00, j00, i10, j10, i01, j01;
						if (flipx) {
							j00 = (src_w - 1) - x;
							j10 = (src_w - 1) - x - ((x + 1) == src_w ? 0 : 1);
							j01 = (src_w - 1) - x;
						} else {
							j00 = x;
							j10 = x + ((x + 1) == src_w ? 0 : 1);
							j01 = x;
						}
						if (flipy) {
							i00 = (src_h - 1) - y;
							i10 = (src_h - 1) - y;
							i01 = (src_h - 1) - y - ((y + 1) == src_h ? 0 : 1);
						} else {
							i00 = y;
							i10 = y;
							i01 = y + ((y + 1) == src_h ? 0 : 1);
						}

						uint32 c00 = src[i00 * src_pitch + j00];
						uint32 c10 = src[i10 * src_pitch + j10];
						uint32 c01 = src[i01 * src_pitch + j01];

						byte rc00, gc00, bc00, ac00;
						byte rc10, gc10, bc10, ac10;
						byte rc01, gc01, bc01, ac01;
						byte r, g, b, a;

						colorDecodeBGRA(c00, &rc00, &gc00, &bc00, &ac00);
						colorDecodeBGRA(c10, &rc10, &gc10, &bc10, &ac10);
						colorDecodeBGRA(c01, &rc01, &gc01, &bc01, &ac01);

						for (dy = 0; dy < gapy; ++dy) {
							for (dx = 0; dx < gapx; ++dx) {
								float32 tx = (float32) dx / (float32) gapx;
								float32 ty = (float32) dy / (float32) gapy;

#if 1
								float32 rx, gx, bx;
								float32 ry, gy, by;
								float32 s = 0.4f;

								rx = ((1.0f - tx) * (float32) rc00 + tx * (float32) rc10);
								ry = ((1.0f - ty) * (float32) rc00 + ty * (float32) rc01);
								r = s * rx + (1.0f - s) * ry;

								gx = ((1.0f - tx) * (float32) gc00 + tx * (float32) gc10);
								gy = ((1.0f - ty) * (float32) gc00 + ty * (float32) gc01);
								g = s * gx + (1.0f - s) * gy;

								bx = ((1.0f - tx) * (float32) bc00 + tx * (float32) bc10);
								by = ((1.0f - ty) * (float32) bc00 + ty * (float32) bc01);
								b = s * bx + (1.0f - s) * by;

								a = ac00;
#else
								r =  (byte) ((1.0f - tx) * (float32) rc00 + tx * (float32) rc10);
								//r = (byte) ((1.0f - ty) * (float32) rc00 + ty * (float32) rc01);

								g = (byte) ((1.0f - tx) * (float32) gc00 + tx * (float32) gc10);
								//g = (byte) ((1.0f - ty) * (float32) gc00 + ty * (float32) gc01);

								b =  (byte) ((1.0f - tx) * (float32) bc00 + tx * (float32) bc10);
								//b = (byte) ((1.0f - ty) * (float32) bc00 + ty * (float32) bc01);

								a = ac00;
#endif
#if 1
								uint idx = (y * gapy + dy) * dst_pitch + (x * gapx + dx);

								uint dst_pixel = dst[idx];
								byte dr, dg, db, da;

								colorDecodeBGRA(dst_pixel, &dr, &dg, &db, &da);

								byte inv_a = 255 - ac00;
								dr = (inv_a * dr + ac00 * r) >> 8;
								dg = (inv_a * dg + ac00 * g) >> 8;
								db = (inv_a * db + ac00 * b) >> 8;
								da = 255;

								uint pixel = colorEncodeBGRA(dr, dg, db, da);
								dst[idx] = pixel;
#else
								uint32 pixel = colorEncodeBGRA(r, g, b, a);
								dst[(y * gapy + dy) * dst_pitch + (x * gapx + dx)] = pixel;
#endif
							}
						}
					} else {
						uint i, j;
						if (flipx) {
							j = (src_w - 1) - x;
						} else {
							j = x;
						}
						if (flipy) {
							i = (src_h - 1) - y;
						} else {
							i = y;
						}

						uint32 c00 = src[i * src_pitch + j];

						for (dy = 0; dy < gapy; ++dy) {
							for (dx = 0; dx < gapx; ++dx) {
#if 1
								uint idx = (y * gapy + dy) * dst_pitch + (x * gapx + dx);

								uint dst_pixel = dst[idx];
								byte sr, sg, sb, sa;
								byte dr, dg, db, da;

								colorDecodeBGRA(dst_pixel, &dr, &dg, &db, &da);
								colorDecodeBGRA(c00, &sr, &sg, &sb, &sa);

								byte inv_a = 255 - sa;
								dr = (inv_a * dr + sa * sr) >> 8;
								dg = (inv_a * dg + sa * sg) >> 8;
								db = (inv_a * db + sa * sb) >> 8;
								da = 255;

								uint pixel = colorEncodeBGRA(dr, dg, db, da);
								dst[idx] = pixel;
#else
								dst[(y * gapy + dy) * dst_pitch + (x * gapx + dx)] = c00;
#endif
							}
						}
					}
				}
			}
		}
	}
}

int main(void) {
	Display *display;
	int screen;
	int window_width, window_height;
	Window window;
	XClassHint *hint;
	Atom protocols, delete_window;

	/* drawing */
	GC gc;
	XImage *back_buffer;
	Texture texture;

	Bool running;

	/* timings */
	struct timespec current_ts, last_ts;
	uint64 current_cycles, last_cycles, delta_cycles;
	int64 delta_nanoseconds, delta_microseconds, delta_milliseconds;
	float32 dt;

	display = XOpenDisplay(NULL);
	screen = XDefaultScreen(display);
	window_width = 1280;
	window_height = 720;
	window = XCreateSimpleWindow(display, XRootWindow(display, screen), 0, 0, window_width, window_height, 0, 0, 0);

	hint = XAllocClassHint();
	hint->res_name  = "X test";
	hint->res_class = "X test";
	XSetClassHint(display, window, hint);
	XStoreName(display, window, hint->res_name);
	XFree(hint);

	gc = XCreateGC(display, window, 0, NULL);
	texture = textureCreateBGRA(window_width, window_height);
	back_buffer = XCreateImage(display, XDefaultVisual(display, screen), XDefaultDepth(display, screen),
		ZPixmap, 0, (char *) texture.pixels, texture.width, texture.height, 32, 0);

	XSelectInput(display, window, StructureNotifyMask | KeyPressMask | KeyReleaseMask | ButtonPressMask | ButtonReleaseMask);

	protocols = XInternAtom(display, "WM_PROTOCOLS", True);
	delete_window = XInternAtom(display, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(display, window, &delete_window, 1);

	XMapWindow(display, window);

	Texture t;
	t = textureLoadPNG("./test.png");

	clock_gettime(CLOCK_MONOTONIC, &current_ts);
	memset(&last_ts, 0, sizeof(last_ts));

	current_cycles = __rdtsc();
	last_cycles = 0;

	uint x = 0;

	running = True;
	while (running) {
		int nevents = XPending(display);
		while (nevents-- > 0) {
			XEvent event;
			XNextEvent(display, &event);
			switch (event.type) {
				case ClientMessage: {
					if (event.xclient.message_type == protocols) {
						Atom protocol = event.xclient.data.l[0];
						if (protocol == delete_window) {
							running = False;
						}
					}
				} break;

				case ConfigureNotify: {
					if (window_width != event.xconfigure.width || window_height != event.xconfigure.height) {
						window_width  = event.xconfigure.width;
						window_height = event.xconfigure.height;

						textureResize(&texture, window_width, window_height);
						back_buffer->width = window_width;
						back_buffer->height = window_height;
						back_buffer->data = (char *) texture.pixels;
						back_buffer->bytes_per_line = window_width * back_buffer->bits_per_pixel / 8;
					}
				} break;

				case KeyPress:
				case KeyRelease: {
					int dummy;
					KeySym *keysyms = XGetKeyboardMapping(display, event.xkey.keycode, 1, &dummy);
					KeySym keysym = keysyms[0];
					XFree(keysyms);

					/*
					printf("%d %c\n", event.xkey.state, keysym);
					*/
				} break;

				default: {
					/*
					printf("%d\n", event.type);
					*/
				} break;
			}
		}

		x = (x + 1) % 400;

		textureFillWithColor(&texture, 33, 33, 33, 255);
		textureFillRectWithColor(&texture, 100, 100, 100, 100, 180, 43, 10, 255);
		textureFillRectWithColor(&texture, 400, 150, 300, 150, 43, 180, 10, 255);
		textureFillRectWithColor(&texture, 150, 400, 150, 300, 43, 10, 150, 255);
		textureFillRectWithColor(&texture, 500, 450, 350, 350, 43, 99, 150, 255);
		textureFillRectWithColor(&texture, 900, 400, 5000, 50, 190, 22, 150, 255);

#if 1
		int pp = 0;
		int p = 0;
		int sx = 10;
		int sy = 10;
		textureFillRectWithTextureScaled(&texture, 100, 100, sx * t.width, sy * t.height,
				                 &t, 0 + pp, 0 + pp, t.width - p, t.height - p, true);

		textureFillRectWithTextureScaled(&texture, 500, 100, sx * t.width, sy * t.height,
				                 &t, 0 + pp, 0 + pp, t.width - p, t.height - p, false);


		textureFillRectWithTextureScaled(&texture, 900, 100, sx * t.width, sx * t.height,
				                 &t, 0, 0, t.width, t.height, false);

#endif

#if 1
		int i, j;
		for (i = 0; i < 40; i++) {
			for (j = 0; j < 25; j++) {
#if 1
				textureFillRectWithTextureBlended(&texture, 25 + (t.width + 0) * i + x, 50 + (t.height + 0) * j, 0, 0, &t);
#else
				textureFillRectWithTexture(&texture, 25 + (t.width + 0) * i + x, 50 + (t.height + 0) * j, t.width, t.height, &t);
				//textureFillRectWithColor(&texture, 25 + (16 + 5) * i + x, 50 + (16 + 5) * j, 16, 16, 128, 0, 0, 255);
#endif
			}
		}
#endif

		XPutImage(display, window, gc, back_buffer, 0, 0, 0, 0, texture.width, texture.height);
		/*
		XFlush(display);
		*/

		delta_cycles = current_cycles - last_cycles;
		delta_nanoseconds = (current_ts.tv_sec - last_ts.tv_sec) * 1e9 + (current_ts.tv_nsec - last_ts.tv_nsec);
		delta_microseconds = delta_nanoseconds / 1e3;
		delta_milliseconds = delta_nanoseconds / 1e6;
		dt = (float32) delta_nanoseconds / 1e9f;

		printf("%.2ffps %lucps %fs %2ldms %5ldmcs %8ldns\n", 1.0f / dt, delta_cycles, dt, delta_milliseconds, delta_microseconds, delta_nanoseconds);

		last_cycles = current_cycles;
		current_cycles = __rdtsc();

		last_ts = current_ts;
		clock_gettime(CLOCK_MONOTONIC, &current_ts);
	}
}
